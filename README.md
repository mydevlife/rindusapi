# Rindus code assessment for java backend engineer
 

### **Assessment description**
Create a small App using Java that consumes a REST API service, using SpringBoot as the base for the project is allowed. Please use as REST service to be consumed the Fake API offered by https://jsonplaceholder.typicode.com/. Implement as many REST Methods as possible in at least one of the resources, e.g. Post, but please call at worst two different resources. 

Extract the data from at least one resource are store it in a JSON File, additionally, you can create an XML file to store the same data.

Please provide information about the decisions taken and how you would improve it in the future, especially considering that this data will be pushed to another service.

### **How to run the application**
- On your terminal execute the command: **`./mvnw spring-boot:run`**
- Application will run on the port: **`8080`**


### **How to run the tests**
On your terminal execute the command: **`mvn test`**

### **Decisions made**
- I decide to have only one controller, since all endpoints provided by jsonplaceholder it has the same structure, only difference is the resource involved. 

- I took advantage of this because this avoid a lot of code duplication, I don’t need to write a controller for each resource. 

- The controller does not perform any logic, just redirect the request to the service layer.

- All the routing is dynamically defined by the ResourceRouterDefiner based on the resource (ResourceData), and in the future more resource will be added we just need to extend the ResourceData, ResourceRoute and create a pojo for it.

- To avoid boilerplate code, I used lomgbok to generate getter, setters and builders.

- For testing I have used Mockito and Junit.

- To simplify the data storage, I decided to store the file locally since the current data size is not that big.

### **Improvements**

- Improve the error handling, to provide more relevant information about the error root cause in a unified format. Use controller advice from spring it will help and simplify the currently error handling mechanism.

- Have and logging trace id mechanism, using aspects for it.

- Main changes I would do in the storage functionality, first thing that we need to consider is to use some external services like aws S3. This is a good choose in case we must maintain/share big files, especially if those files should accessible to other services. In that case we would need to refactor our repository layer to support S3.

- Right now, service is wrapping the output in a ResponseEntity, I did this in the very beginning to avoid put some logic in the controller, my Idea was to keep the controller as clean as possible to just call the service layer. This may it not the best approach, it would be better to return the result as objects this would make your service more reusable in case other services (if there was such) need to use it. But for the moment it’s ok because this service talks only with the controller. I didn’t have time to implement/refactor this, but I have it mind.

- Another point is, I believe don’t need the ResourceRoute, we could add this information in the ResourceData and there’s we deal with it. But again, I didn’t have time for this.

- Currently every time that we call the data/resourceName/json we override the existing file and create a new one. This is not performic, first I would create and time to leave mechanism (or something similar) for the files that will avoid multiple file creations. If the file still “a live” I would just return it, if not we do a refresh data and return it.

- I would consider creating a read file functionality, that would display files content (json or xml) and the list of existing files.

- I would consider using swagger as well, to provide api documentation.

- In case we were working with a real api, I would consider a cache system if we would need to handle massive amount of data.





### **Resources available**
|  |  |
| ------ | ------ |
| /posts | 100 posts |
| /comments | 500 comments |
| /albums | 100 albums |
| /photos | 5000 photos |
| /todos | 200 todos |

### **Endpoints available**
Below Endpoints are applicable to all resources, we are using posts to exemplify.

| Http Method | Endpoint | What does? |
| ------ | ------ | ------ |
| GET | /posts | retrieve all posts |
| GET | /posts/1 | retrieve post which has id = 1 |
| POST | /posts | Creates a post based on the request body and return it |
| PUT | /posts/1 | Updates a post based on the request body and return it |
| DELETE | /posts/1 | Deletes a post which has id = 1 and return empty responso in case of success |
| POST | /data/posts/json | Creates an `json` file under the resources folder with all posts |
| POST | /data/posts/xml | Creates an `xml` file under the resources folder with all posts |

### **Storing data in a file**
To store data in a file you need to access below endpoint and inform the resource from that you would like to get the data that will be stored in the file, additionally you need provide the file format (only xml and json are supported).

If you provide a different format, and error message will be returned: `Not Valid file format`

In case of success the Absolut path of the file will be returned: `C:\Users\Lucas\IdeaProjects\rindusApi\src\main\resources\TODOS.json`

- `http://localhost:8080/data/todos/json`
- `http://localhost:8080/data/todos/xml`

**_If you would like to test the storing data or any other post methods please use curl command or Postman._**


