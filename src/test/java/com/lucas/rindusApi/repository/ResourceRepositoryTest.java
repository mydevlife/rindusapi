package com.lucas.rindusApi.repository;

import com.lucas.rindusApi.model.Post;
import com.lucas.rindusApi.model.ResourceData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
public class ResourceRepositoryTest {

    @Autowired
    private ResourceRepository repository;

    @Value("${save.file.dir}")
    private String expectedFileLocation;

    @Test
    public void contextLoads() throws Exception {
        assertThat(repository).isNotNull();
    }

    @Test
    public void saveJsonFileTest() throws IOException {
        final String json = "json";
        final Post newPost = createPost();

        final ResponseEntity<Object> data = wrapperIntoSuccessfulResponse(Arrays.asList(newPost));
        String location = repository.saveFile(data, ResourceData.POSTS.getRoute().name(), json);
        assertFileExistsAndDeleteFile(location, json);
    }

    @Test
    public void saveXmlFileTest() throws IOException {
        final String xml = "xml";
        final Post newPost = createPost();

        final ResponseEntity<Object> data = wrapperIntoSuccessfulResponse(Arrays.asList(newPost));
        String location = repository.saveFile(data, ResourceData.POSTS.getRoute().name(), xml);
        assertFileExistsAndDeleteFile(location, xml);
    }

    @Test
    public void saveInvalidFormatFileTest() {
        final String xml = "doc";
        final Post newPost = createPost();

        final ResponseEntity<Object> data = wrapperIntoSuccessfulResponse(Arrays.asList(newPost));
        try {
            repository.saveFile(data, ResourceData.POSTS.getRoute().name(), xml);
            Assertions.fail();
        } catch (Exception e) {
            assertThat(e).isInstanceOf(IllegalArgumentException.class);
        }

    }

    private void assertFileExistsAndDeleteFile(String location, String fileFormat) {
        assertThat(location).isNotNull();
        String expectedLocationWithOutSeparator = expectedFileLocation.concat(ResourceData.POSTS.getRoute().name().concat(".").concat(fileFormat))
                .replaceAll("/", "").replaceAll("\\\\", "");
        String locationWithOutSeparator = location.replaceAll("/", "").replaceAll("\\\\", "");

        assertThat(locationWithOutSeparator.endsWith(expectedLocationWithOutSeparator)).isTrue();

        try {
            Files.deleteIfExists(Paths.get(location));
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    private Post createPost() {
       return Post.builder()
                .id(1)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();
    }

}
