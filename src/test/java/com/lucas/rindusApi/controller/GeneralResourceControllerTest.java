package com.lucas.rindusApi.controller;

import com.lucas.rindusApi.model.Post;
import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.service.ResourceDataService;
import com.lucas.rindusApi.service.ResourceService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static com.lucas.rindusApi.util.Parser.fromObjToJson;
import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GeneralResourceController.class)
@ActiveProfiles("test")
public class GeneralResourceControllerTest {

    @MockBean
    private ResourceService resourceService;

    @MockBean
    private ResourceDataService dataService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllTest() throws Exception {
        final Post expectedPost = createPost();

        Mockito.when(resourceService.getAll(ResourceData.POSTS_LIST)).thenReturn(wrapperIntoSuccessfulResponse(Arrays.asList(expectedPost)));

        final MvcResult result = this.mockMvc.perform(get("/posts")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        assertThat(content).isEqualTo(fromObjToJson(Arrays.asList(expectedPost)));
    }

    @Test
    public void getByIdTest() throws Exception {
        final Post expectedPost = createPost();

        Mockito.when(resourceService.getById(ResourceData.POSTS, expectedPost.getId())).thenReturn(wrapperIntoSuccessfulResponse(expectedPost));

        final MvcResult result = this.mockMvc.perform(get("/posts/"+expectedPost.getId())
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        assertThat(content).isEqualTo(fromObjToJson(expectedPost));
    }

    @Test
    public void deleteTest() throws Exception {
        final ResponseEntity expected = new ResponseEntity<>("{}", HttpStatus.OK);

        Mockito.when(resourceService.delete(Mockito.any(ResourceData.class), Mockito.anyInt())).thenReturn(expected);

        final MvcResult result = this.mockMvc.perform(delete("/posts/1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();


        assertThat(result.getResponse().getStatus()).isEqualTo(expected.getStatusCode().value());
        assertThat(result.getResponse().getContentAsString()).isEqualTo(expected.getBody());
    }

    @Test
    public void createTest() throws Exception {
        final Post newPost = createPost();

        Mockito.when(resourceService.create(Mockito.any(ResourceData.class), Mockito.any(Object.class))).thenReturn(wrapperIntoSuccessfulResponse(newPost));

        final MvcResult result = this.mockMvc.perform(
                post("/posts")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(fromObjToJson(newPost))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        assertThat(content).isEqualTo(fromObjToJson(newPost));
    }

    @Test
    public void updateTest() throws Exception {
        final Post newPost = createPost();

        Mockito.when(resourceService.update(Mockito.any(ResourceData.class), Mockito.any(Object.class), Mockito.anyInt())).thenReturn(wrapperIntoSuccessfulResponse(newPost));

        final MvcResult result = this.mockMvc.perform(
                put("/posts/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(fromObjToJson(newPost))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();

        final String content = result.getResponse().getContentAsString();
        assertThat(content).isEqualTo(fromObjToJson(newPost));
    }

    @Test
    public void getByIdWhenIdIsNotValidTest() throws Exception {
        Mockito.when(resourceService.getById(ResourceData.POSTS, 1)).thenReturn(new ResponseEntity<>("{}", HttpStatus.BAD_REQUEST));
        this.mockMvc.perform(get("/posts/xpto")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAllWhenResourceNotValidTest() throws Exception {
        Mockito.when(resourceService.getAll(Mockito.any())).thenReturn(new ResponseEntity<>("{}", HttpStatus.NOT_FOUND));
        this.mockMvc.perform(get("/random")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateWithoutIdTest() throws Exception {
        this.mockMvc.perform(put("/posts")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void deleteWithoutIdTest() throws Exception {
        this.mockMvc.perform(delete("/posts")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void createWithIdTest() throws Exception {
        this.mockMvc.perform(post("/posts/1")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void saveFileAsJsonTest() throws Exception {
        this.mockMvc.perform(post("/data/posts/json")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void saveFileAsXmlTest() throws Exception {
        this.mockMvc.perform(post("/data/posts/xml")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    private Post createPost() {
        return Post.builder()
                .id(1)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();
    }

}
