package com.lucas.rindusApi.util;

import com.lucas.rindusApi.exception.ClientException;
import com.lucas.rindusApi.exception.InternalServerException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mock.http.client.MockClientHttpResponse;

import static org.assertj.core.api.Assertions.assertThat;

public class ResourceResponseErrorHandlerTest {

    ResourceResponseErrorHandler resourceResponseErrorHandler = ResourceResponseErrorHandler.getInstance();

    @Test
    public void handleRestTemplateErrorNotFoundTest() {
        final ResponseEntity<Object> result = ResourceResponseErrorHandler.handleRestTemplateError(new ResourceNotFoundException());
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(result.getBody()).isEqualTo("Resource was not found");
    }

    @Test
    public void handleRestTemplateBadRequestTest() {
        final ResponseEntity<Object> result = ResourceResponseErrorHandler.handleRestTemplateError(new ClientException());
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(result.getBody()).isEqualTo("Request placed contains problems");
    }

    @Test
    public void handleRestTemplateInternalServerErrorTest() {
        final ResponseEntity<Object> result = ResourceResponseErrorHandler.handleRestTemplateError(new InternalServerException());
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result.getBody()).isEqualTo("Something went wrong");
    }

    @Test
    public void handleRestTemplateNotFoundResourcesTest() {
        final ResponseEntity<Object> result = ResourceResponseErrorHandler.handleRestTemplateError(new NullPointerException());
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(result.getBody()).isEqualTo("Resource is not valid, for more info visit: jsonplaceholder.typicode.com");
    }

    @Test
    public void handleErrorInternalServerTest() {
        final ClientHttpResponse clientHttpResponse = new MockClientHttpResponse(new byte[] {}, HttpStatus.INTERNAL_SERVER_ERROR);
        try{
            resourceResponseErrorHandler.handleError(clientHttpResponse);
            Assertions.fail();
        } catch (Exception e) {
            assertThat(e).isInstanceOf(InternalServerException.class);
        }
    }

    @Test
    public void handleErrorNotFoundTest() {
        final ClientHttpResponse clientHttpResponse = new MockClientHttpResponse(new byte[] {}, HttpStatus.NOT_FOUND);
        try{
            resourceResponseErrorHandler.handleError(clientHttpResponse);
            Assertions.fail();
        } catch (Exception e) {
            assertThat(e).isInstanceOf(ResourceNotFoundException.class);
        }
    }

    @Test
    public void handleErrorClientTest() {
        final ClientHttpResponse clientHttpResponse = new MockClientHttpResponse(new byte[] {}, HttpStatus.BAD_REQUEST);
        try{
            resourceResponseErrorHandler.handleError(clientHttpResponse);
            Assertions.fail();
        } catch (Exception e) {
            assertThat(e).isInstanceOf(ClientException.class);
        }
    }
}
