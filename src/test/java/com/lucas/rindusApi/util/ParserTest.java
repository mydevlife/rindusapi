package com.lucas.rindusApi.util;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.lucas.rindusApi.model.Post;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {

    private final String expectedJson = "{\"headers\":{},\"body\":[{\"id\":1,\"userId\":1,\"title\":\"Lucas post Title\",\"body\":\"Rindus rocks!\"},{\"id\":2,\"userId\":1,\"title\":\"Lucas post Title\",\"body\":\"Rindus rocks!\"}],\"statusCode\":\"OK\",\"statusCodeValue\":200}";
    private final String expectedXml = "<ResponseEntity><headers/><body><id>1</id><userId>1</userId><title>Lucas post Title</title><body>Rindus rocks!</body></body><body><id>2</id><userId>1</userId><title>Lucas post Title</title><body>Rindus rocks!</body></body><statusCode>OK</statusCode><statusCodeValue>200</statusCodeValue></ResponseEntity>";

    @Test
    public void fromObjToJsonTest() throws JsonProcessingException {
        final String json = Parser.fromObjToJson(createPostsResponse());
        assertThat(json.toString()).isEqualTo(expectedJson);
    }

    @Test
    public void fromObjToXmlTest() throws JsonProcessingException {
        final String xml = Parser.fromObjToXml(createPostsResponse());
        assertThat(xml.toString()).isEqualTo(expectedXml);
    }

    private ResponseEntity<Object> createPostsResponse() {
        final Post post1 = Post.builder()
                .id(1)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();

        final Post post2 = Post.builder()
                .id(2)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();

        return wrapperIntoSuccessfulResponse(Arrays.asList(post1, post2));
    }
}
