package com.lucas.rindusApi.util;

import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.model.ResourceRoute;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ResourceRouteDefinerTest {

    @Test
    public void getByPathTest() {
        ResourceData resourceData = ResourceRouteDefiner.getByPath(ResourceRoute.POSTS.name().toLowerCase());
        assertThat(resourceData).isEqualTo(ResourceData.POSTS);

        resourceData = ResourceRouteDefiner.getByPath(ResourceRoute.COMMENTS.name().toLowerCase());
        assertThat(resourceData).isEqualTo(ResourceData.COMMENTS);
    }

    @Test
    public void getByPathListTest() {
        ResourceData resourceData = ResourceRouteDefiner.getByPath(ResourceRoute.POSTS.name().toLowerCase(), true);
        assertThat(resourceData).isEqualTo(ResourceData.POSTS_LIST);

        resourceData = ResourceRouteDefiner.getByPath(ResourceRoute.COMMENTS.name().toLowerCase(), true);
        assertThat(resourceData).isEqualTo(ResourceData.COMMENTS_LIST);
    }

    @Test
    public void getByPathNotValidResourceTest() {
        final ResourceData resourceData = ResourceRouteDefiner.getByPath("random", true);
        assertThat(resourceData).isNull();
    }
}
