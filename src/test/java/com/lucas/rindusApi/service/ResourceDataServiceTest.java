package com.lucas.rindusApi.service;

import com.lucas.rindusApi.model.Post;
import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.repository.ResourceRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Arrays;

import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ResourceDataServiceTest {

    @Mock
    private ResourceService resourceService;

    @Mock
    private ResourceRepository repository;

    @InjectMocks
    private ResourceDataService dataService = new ResourceDataServiceImpl(repository, resourceService);

    @Test
    public void saveFile() throws IOException {
        Post newPost = Post.builder()
                .id(1)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();

        ResponseEntity<Object> data = wrapperIntoSuccessfulResponse(Arrays.asList(newPost));

        Mockito.when(resourceService.getAll(ResourceData.POSTS_LIST))
                .thenReturn(data);

        Mockito.when(repository.saveFile(data, ResourceData.POSTS.getRoute().name(), "json"))
                .thenReturn("src/resources/posts.json");

        ResponseEntity<Object> fileLocation = wrapperIntoSuccessfulResponse(dataService.save(ResourceData.POSTS, "json"));
        assertThat(fileLocation.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
