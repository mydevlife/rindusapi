package com.lucas.rindusApi.service;

import com.lucas.rindusApi.exception.ClientException;
import com.lucas.rindusApi.model.Post;
import com.lucas.rindusApi.model.ResourceData;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ResourceServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    ResourceService resourceService = new ResourceServiceImpl(restTemplate);

    private final String URL = "https://jsonplaceholder.typicode.com/posts";

    @Test
    public void getByIdTest() {
        final Post post = crearePost();

        final ResponseEntity<Object> responseEntityExpected = new ResponseEntity<>(post, HttpStatus.OK);

        Mockito.when(restTemplate.getForObject(URL + "/1", Post.class))
                .thenReturn(post);

        final ResponseEntity<Object> result = resourceService.getById(ResourceData.POSTS, post.getId());

        assertThat(result).isInstanceOf(responseEntityExpected.getClass());
        assertThat(result).isEqualTo(responseEntityExpected);
    }

    @Test
    public void getByIdNotFoundTest() {
        final ResponseEntity<Object> responseEntityExpected = new ResponseEntity<>("Resource was not found", HttpStatus.NOT_FOUND);

        Mockito.when(restTemplate.getForObject(URL + "/1234", Post.class))
                .thenThrow(new ResourceNotFoundException());

        final ResponseEntity<Object> result = resourceService.getById(ResourceData.POSTS, 1234);

        assertThat(result).isInstanceOf(responseEntityExpected.getClass());
        assertThat(result).isEqualTo(responseEntityExpected);
    }

    @Test
    public void getByIdBadRequestTest() {
        final ResponseEntity<Object> responseEntityExpected = new ResponseEntity<>("Request placed contains problems", HttpStatus.BAD_REQUEST);

        Mockito.when(restTemplate.getForObject(URL + "/-1", Post.class))
                .thenThrow(new ClientException());

        final ResponseEntity<Object> result = resourceService.getById(ResourceData.POSTS, -1);

        assertThat(result).isInstanceOf(responseEntityExpected.getClass());
        assertThat(result).isEqualTo(responseEntityExpected);
    }

    @Test
    public void createTest() {
        final Post post = crearePost();

        final ResponseEntity<Object> responseEntityExpected = new ResponseEntity<>(post, HttpStatus.OK);

        Mockito.when(restTemplate.postForObject(URL, new HttpEntity<>(post), Post.class))
                .thenReturn(post);

        final ResponseEntity<Object> result = resourceService.create(ResourceData.POSTS, post);

        assertThat(result).isInstanceOf(responseEntityExpected.getClass());
        assertThat(result).isEqualTo(responseEntityExpected);
    }

    @Test
    public void updateTest() {
        final Post post = crearePost();

        final ResponseEntity<Post> responseEntityExpected = new ResponseEntity<>(post, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(URL+"/"+post.getId(), HttpMethod.PUT, new HttpEntity<>(post), Post.class))
                .thenReturn(responseEntityExpected);

        final ResponseEntity<Object> result = resourceService.update(ResourceData.POSTS, post, post.getId());

        assertThat(result).isInstanceOf(responseEntityExpected.getClass());
        assertThat(result).isEqualTo(responseEntityExpected);
    }

    private Post crearePost() {
        return Post.builder()
                .id(1)
                .title("Lucas post Title")
                .userId(1)
                .body("Rindus rocks!").build();
    }
}
