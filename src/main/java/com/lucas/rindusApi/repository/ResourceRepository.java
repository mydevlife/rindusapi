package com.lucas.rindusApi.repository;

import java.io.IOException;

public interface ResourceRepository {

    public static final String JSON_FORMAT = "json";
    public static final String XML_FORMAT = "xml";

    String saveFile(Object data, String fileName, String fileFormat) throws IOException;
}
