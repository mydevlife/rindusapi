package com.lucas.rindusApi.repository;

import com.lucas.rindusApi.util.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class ResourceRepositoryImpl implements ResourceRepository {

    private final Logger logger = LoggerFactory.getLogger(ResourceRepositoryImpl.class);

    @Value("${save.file.dir}")
    private String savePath;

    @Override
    public String saveFile(Object resources, String fileName, String fileFormat) throws IOException {

        validateFileFormat(fileFormat);

        Path file = initializerFile(fileName, fileFormat);

        write(file, fileFormat, resources);

        String location = file.toAbsolutePath().toString();
        logger.debug("File {} was created at {}", fileName, location);
        return location;
    }

    private Path initializerFile(String fileName, String fileFormat) throws IOException {
        logger.debug("Creating new file {}",fileName);
        Path file = Paths.get(savePath.concat(fileName).concat(".").concat(fileFormat));
        Files.createDirectories(file.getParent());
        return file;
    }

    private void write(Path file, String fileFormat, Object data) throws IOException {
        if (fileFormat.equals(ResourceRepository.XML_FORMAT)) {
            logger.debug("writing XML file");
            Files.write(file, Parser.fromObjToXml(data).getBytes());
        } else {
            logger.debug("writing JSON file");
            Files.write(file, Parser.fromObjToJson(data).getBytes());
        }
    }

    private void validateFileFormat(String fileFormat) {
        if (fileFormat == null ||
                !(fileFormat.equalsIgnoreCase(ResourceRepository.JSON_FORMAT)
                        || fileFormat.equalsIgnoreCase(ResourceRepository.XML_FORMAT))) {
            throw new IllegalArgumentException("Not Valid file format");
        }
    }
}
