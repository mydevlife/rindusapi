package com.lucas.rindusApi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Comment {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private Integer postId;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String body;

    public Comment(){
    };

    public Comment(Integer id, Integer postId, String email, String name, String body) {
        this.id = id;
        this.postId = postId;
        this.email = email;
        this.name = name;
        this.body = body;
    }
}
