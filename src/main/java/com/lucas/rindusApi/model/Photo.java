package com.lucas.rindusApi.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class Photo {

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Integer albumId;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String url;
    @Getter
    @Setter
    private String thumbnailUrl;

    public Photo(){};

    public Photo(Integer id, Integer albumId, String title, String url, String thumbnailUrl) {
        this.id = id;
        this.albumId = albumId;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }
}
