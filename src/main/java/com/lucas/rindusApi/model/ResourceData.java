package com.lucas.rindusApi.model;

public enum ResourceData {

    POSTS(Post.class, ResourceRoute.POSTS),
    POSTS_LIST(Post[].class, true, ResourceRoute.POSTS),
    COMMENTS(Comment.class, ResourceRoute.COMMENTS),
    COMMENTS_LIST(Comment[].class, true, ResourceRoute.COMMENTS),
    ALBUMS(Album.class, false, ResourceRoute.ALBUMS),
    ALBUMS_LIST(Album[].class, true, ResourceRoute.ALBUMS),
    PHOTOS(Photo.class, false, ResourceRoute.PHOTOS),
    PHOTOS_LIST(Photo[].class, true, ResourceRoute.PHOTOS),
    TODOS(Todo.class, false, ResourceRoute.TODOS),
    TODOS_LIST(Todo[].class, true, ResourceRoute.TODOS);

    private final Class<?> type;
    private final boolean isList;
    private final ResourceRoute route;

    ResourceData(Class<?> type, boolean isList, ResourceRoute route) {
        this.type = type;
        this.isList = isList;
        this.route = route;
    }

    ResourceData(Class<?> type, ResourceRoute route) {
        this.type = type;
        this.isList = false;
        this.route = route;
    }

    public Class<?> getType() {
        return type;
    }

    public boolean isList() {
        return isList;
    }

    public String getUrl() {
        return route.url();
    }

    public ResourceRoute getRoute() {
        return route;
    }

}
