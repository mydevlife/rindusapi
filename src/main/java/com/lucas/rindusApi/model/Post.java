package com.lucas.rindusApi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Post {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private Integer userId;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String body;

    public Post(){};

    public Post(Integer id, Integer userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

}
