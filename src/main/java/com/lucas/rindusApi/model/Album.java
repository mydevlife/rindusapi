package com.lucas.rindusApi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Album {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private Integer userId;

    @Getter
    @Setter
    private String title;

    public Album() {};

    public Album(Integer id, Integer userId, String title) {
        this.id = id;
        this.userId = userId;
        this.title = title;
    }
}
