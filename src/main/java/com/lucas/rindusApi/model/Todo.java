package com.lucas.rindusApi.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class Todo {
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Integer userId;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String completed;

    public Todo(){};

    public Todo(Integer id, Integer userId, String title, String completed) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.completed = completed;
    }
}
