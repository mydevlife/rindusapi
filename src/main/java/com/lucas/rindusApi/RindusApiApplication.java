package com.lucas.rindusApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RindusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RindusApiApplication.class, args);
	}

}
