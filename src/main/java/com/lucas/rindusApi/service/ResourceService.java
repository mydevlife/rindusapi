package com.lucas.rindusApi.service;

import com.lucas.rindusApi.model.ResourceData;
import org.springframework.http.ResponseEntity;

public interface ResourceService {

    ResponseEntity<Object> getAll(final ResourceData resource);

    ResponseEntity<Object> getById(final ResourceData resource, final Integer id);

    ResponseEntity<Object> create(final ResourceData resource, final Object object);

    ResponseEntity<Object> update(final ResourceData resource, final Object object, final Integer id);

    ResponseEntity<Object> delete(final ResourceData resource, final Integer id);
}
