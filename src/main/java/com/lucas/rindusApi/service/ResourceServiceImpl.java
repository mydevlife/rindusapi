package com.lucas.rindusApi.service;

import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.util.ResourceResponseErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private HttpHeaders headers;

    private RestTemplate restTemplate;

    @Autowired
    public ResourceServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<Object> getAll(final ResourceData resourceData) {
        try {
            return wrapperIntoSuccessfulResponse(restTemplate.getForObject(resourceData.getUrl(), resourceData.getType()));
        } catch (RuntimeException e) {
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

    @Override
    public ResponseEntity<Object> getById(final ResourceData resourceData, final Integer id) {
        try {
            return wrapperIntoSuccessfulResponse(restTemplate.getForObject(resourceData.getUrl() + "/" + id, resourceData.getType()));
        } catch (RuntimeException e) {
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

    @Override
    public ResponseEntity<Object> create(final ResourceData resourceData, final Object object) {
        try {
            return wrapperIntoSuccessfulResponse(restTemplate.postForObject(resourceData.getUrl(), new HttpEntity<>(object, headers), resourceData.getType()));
        } catch (RuntimeException e) {
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

    @Override
    public ResponseEntity<Object> update(final ResourceData resourceData, final Object entity, final Integer id) {
        try {
            final String endpoint = resourceData.getUrl().concat("/"+id);
            final HttpEntity httpEntity = new HttpEntity<>(entity, headers);
            return wrapperIntoSuccessfulResponse(restTemplate.exchange(endpoint, HttpMethod.PUT, httpEntity, resourceData.getType()));
        } catch (RuntimeException e) {
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

    @Override
    public ResponseEntity<Object> delete(final ResourceData resourceData, final Integer id) {
        try {
            final String endpoint = resourceData.getUrl().concat("/"+id);
            restTemplate.delete(endpoint);
            return wrapperIntoSuccessfulResponse(null);
        } catch (RuntimeException e) {
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

}
