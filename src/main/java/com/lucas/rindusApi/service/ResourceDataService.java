package com.lucas.rindusApi.service;

import com.lucas.rindusApi.model.ResourceData;
import org.springframework.http.ResponseEntity;

public interface ResourceDataService {

    public ResponseEntity<Object> save(ResourceData resourceData, String fileFormat);
}
