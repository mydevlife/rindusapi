package com.lucas.rindusApi.service;

import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.repository.ResourceRepository;
import com.lucas.rindusApi.util.ResourceResponseErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import static com.lucas.rindusApi.util.ResponseWrapper.wrapperIntoSuccessfulResponse;

@Service
public class ResourceDataServiceImpl implements ResourceDataService {

    private final Logger logger = LoggerFactory.getLogger(ResourceDataServiceImpl.class);

    private ResourceRepository repository;
    private ResourceService service;

    @Autowired
    public ResourceDataServiceImpl(ResourceRepository repository, ResourceService service) {
        this.repository = repository;
        this.service = service;
    }

    @Override
    public ResponseEntity<Object> save(ResourceData resourceData, String fileFormat){
        try {
            logger.debug("Getting all {}", resourceData.name());
            final ResponseEntity<Object> data = service.getAll(resourceData);
            final String location = repository.saveFile(data, resourceData.getRoute().name(), fileFormat);
            return wrapperIntoSuccessfulResponse(location);
        } catch (Exception e){
            return ResourceResponseErrorHandler.handleRestTemplateError(e);
        }
    }

}
