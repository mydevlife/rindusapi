package com.lucas.rindusApi.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseWrapper {

    public static ResponseEntity<Object> wrapperIntoSuccessfulResponse(final Object resource) {
        if (resource instanceof ResponseEntity<?>) {
            return (ResponseEntity<Object>) resource;
        }
        return resource == null ? new ResponseEntity<>("{}", HttpStatus.OK) : new ResponseEntity<>(resource, HttpStatus.OK);
    }
}
