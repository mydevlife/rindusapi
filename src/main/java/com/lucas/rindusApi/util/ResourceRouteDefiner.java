package com.lucas.rindusApi.util;

import com.lucas.rindusApi.model.ResourceData;
import com.lucas.rindusApi.model.ResourceRoute;

import java.util.Arrays;

public class ResourceRouteDefiner {

    public static ResourceData getByPath(String path) {
        return getByRoute(ResourceRoute.getByPath(path), false);
    }

    public static ResourceData getByPath(String resourceName, boolean list) {
        return getByRoute(ResourceRoute.getByPath(resourceName), list);
    }

    private static ResourceData getByRoute(ResourceRoute route, boolean list) {
        if (route == null) {
            return null;
        }
        String name = list ? route.name() + "_LIST" : route.name();
        return Arrays.stream(ResourceData.values()).filter(resource -> resource.name().equals(name))
                .findFirst().orElse(null);
    }
}
