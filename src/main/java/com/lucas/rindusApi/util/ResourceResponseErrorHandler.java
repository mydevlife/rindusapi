package com.lucas.rindusApi.util;

import com.lucas.rindusApi.exception.ClientException;
import com.lucas.rindusApi.exception.InternalServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class ResourceResponseErrorHandler implements ResponseErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(ResponseErrorHandler.class);
    private static ResourceResponseErrorHandler instance;

    public static ResourceResponseErrorHandler getInstance() {
        if(instance == null) {
            instance = new ResourceResponseErrorHandler();
        }
        return instance;
    }

    private ResourceResponseErrorHandler(){};

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return HttpStatus.Series.CLIENT_ERROR == clientHttpResponse.getStatusCode().series()
                || HttpStatus.Series.SERVER_ERROR == clientHttpResponse.getStatusCode().series();
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        if (clientHttpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            throw new InternalServerException();
        } else if (clientHttpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
            if (clientHttpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new ResourceNotFoundException();
            }
            throw new ClientException();
        }
    }

    public static ResponseEntity<Object> handleRestTemplateError(Exception exception) {

        logger.debug("Something wrong happened: {}", exception.getClass().getSimpleName());

        if (exception instanceof ResourceNotFoundException) {
            final String errorMsg = "Resource was not found";
            logger.debug("{} {}", errorMsg, HttpStatus.NOT_FOUND);
            return new ResponseEntity<>( errorMsg, HttpStatus.NOT_FOUND);

        } else if (exception instanceof ClientException) {
            final String errorMsg = "Request placed contains problems";
            logger.debug("returning {}", HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);

        } else if (exception instanceof InternalServerException) {
            final String errorMsg = "Something went wrong";
            logger.debug("returning {}", HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);

        } else if (exception instanceof NullPointerException) {
            final String errorMsg = "Resource is not valid, for more info visit: jsonplaceholder.typicode.com";
            logger.debug("{} {}", errorMsg,  HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(errorMsg, HttpStatus.NOT_FOUND);

        } else if(exception instanceof IOException) {
            final String errorMsg = "Error during the file manipulation";
            logger.debug("{} {}", errorMsg,  HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
