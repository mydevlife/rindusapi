package com.lucas.rindusApi.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lucas.rindusApi.service.ResourceDataServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Parser {
    private static final Logger logger = LoggerFactory.getLogger(ResourceDataServiceImpl.class);
    private static ObjectMapper jsonMapper;
    private static XmlMapper xmlMapper;

    static {
        jsonMapper = new ObjectMapper();
        xmlMapper = new XmlMapper();
    }

    public static String fromObjToXml(Object obj) throws JsonProcessingException {
        logger.debug("Parsing from object to xml");
        return xmlMapper.writeValueAsString(obj);
    }

    public static String fromObjToJson(Object obj) throws JsonProcessingException {
        logger.debug("Parsing from object to json");
        return jsonMapper.writeValueAsString(obj);
    }

}
