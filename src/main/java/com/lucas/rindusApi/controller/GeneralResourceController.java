package com.lucas.rindusApi.controller;

import com.lucas.rindusApi.service.ResourceDataService;
import com.lucas.rindusApi.service.ResourceService;
import com.lucas.rindusApi.util.ResourceRouteDefiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GeneralResourceController {

    private final String JSON = MediaType.APPLICATION_JSON_VALUE;
    private ResourceService resourceService;
    private ResourceDataService dataService;

    @Autowired
    public GeneralResourceController(ResourceService resourceService, ResourceDataService dataService){
        this.resourceService = resourceService;
        this.dataService = dataService;
    }

    @GetMapping(value = "/{resourceName}", produces = JSON)
    @ResponseBody
    public ResponseEntity<Object> getAll(@PathVariable String resourceName) {
        return resourceService.getAll(ResourceRouteDefiner.getByPath(resourceName.toLowerCase(), true));
    }

    @GetMapping(value = "/{resourceName}/{id}", produces = JSON)
    @ResponseBody
    public ResponseEntity<Object> getById(@PathVariable String resourceName, @PathVariable Integer id) {
        return resourceService.getById(ResourceRouteDefiner.getByPath(resourceName.toLowerCase()), id);
    }

    @PostMapping(value = "/{resourceName}", produces = JSON, consumes = JSON)
    @ResponseBody
    public ResponseEntity<Object> create(@PathVariable String resourceName, @RequestBody Object newResource) {
        return resourceService.create(ResourceRouteDefiner.getByPath(resourceName.toLowerCase()), newResource);
    }

    @PutMapping(value = "/{resourceName}/{id}", produces = JSON, consumes = JSON)
    public ResponseEntity<Object> update(@PathVariable String resourceName, @RequestBody Object resource, @PathVariable Integer id) {
        return resourceService.update(ResourceRouteDefiner.getByPath(resourceName.toLowerCase()), resource, id);
    }

    @DeleteMapping(value = "/{resourceName}/{id}", produces = JSON)
    public ResponseEntity<Object> delete(@PathVariable String resourceName, @PathVariable Integer id) {
        return resourceService.delete(ResourceRouteDefiner.getByPath(resourceName.toLowerCase()), id);
    }

    @PostMapping(value = "/data/{resourceName}/{fileFormat}")
    @ResponseBody
    public ResponseEntity<Object> create(@PathVariable String resourceName, @PathVariable String fileFormat) {
        return dataService.save(ResourceRouteDefiner.getByPath(resourceName.toLowerCase(), true), fileFormat);
    }
}
